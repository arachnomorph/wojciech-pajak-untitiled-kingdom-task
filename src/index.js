const fs = require("fs");
const path = require("path");

(function init() {
  const users = JSON.parse(fs.readFileSync(path.resolve(__dirname, "../data/users.json"), "utf-8"));
  const mobileDevices = JSON.parse(fs.readFileSync(path.resolve(__dirname, "../data/mobile_devices.json"), "utf-8"));
  const iotDevices = JSON.parse(fs.readFileSync(path.resolve(__dirname, "../data/iot_devices.json"), "utf-8"));

  console.log(new Date().toISOString());
  console.log(count(users, mobileDevices, iotDevices));
  console.log(new Date().toISOString());
})();

function count(users, mobileDevices, iotDevices) {

  const iotPerDevice = mobileDevices.map(mobile => {
    const mobilesIots = iotDevices.filter(iot => iot.mobile === mobile.id)

    return { ...mobile, numOfIots: mobilesIots.length }
  })

  const iotsPerUser = users.map(user => {
    const usersDevices = iotPerDevice
      .filter(device => device.user === user.id)
      .reduce((prev, curr) => prev + curr.numOfIots, 0)

    return { ...user, numOfIots: usersDevices }
  })

  // // pretty output
  // let stringifiedOutput = iotsPerUser.map(user => {
  //   return `- All users named ${user.name.split(' ').splice(0, 1)} owned ${user.numOfIots} ${user.numOfIots === 1 ? 'device' : 'devices'} in total,`
  // }).join('\n').slice(0, -1)

  return iotsPerUser
  // return stringifiedOutput
}
